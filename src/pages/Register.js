import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { useNavigate, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);

	// State hooks to store values of the input fields
	const [email, setEmail] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function registerUser(e){

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			body: JSON.stringify({
				email: email
			}),
			headers: {
				'Content-Type': 'application/json'
			}
		})
		.then(res => res.json())
		.then(data => {
			if (data === true){
				Swal.fire({
					title: "Duplicate email found",
					icon: "error",
					text: "Please provide a different email"
				})
			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					body: JSON.stringify({
						email: email,
						password: password1,
						firstName: firstName,
						lastName: lastName,
						mobileNo: mobileNo
					}),
					headers: {
						'Content-Type': 'application/json'
					}
				})
				.then(res => res.json())
				.then(data => {
					Swal.fire({
						title: "Registration successful",
						icon: "success",
						text: "Welcome to Zuitt!"
					})
					navigate("/login");
				});
			};
		});

		setEmail("");
		setPassword1("");
		setPassword2("");
		setFirstName("");
		setLastName("")
		setMobileNo("");
	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both password matches

		if((email !== "" && password1 !== "" && password2 !== "") && (password1 === password2) && (password1.length && password2.length >= 8) && (firstName.length && lastName.length && mobileNo.length >= 11)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password1, password2, firstName, lastName, mobileNo]);

	//useEffect(() => {
	//	navigate('/courses');
	//}, []);

	return (
		(user.id !== null) ?
		<Navigate to="/login" />
		:
		<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
				<Form.Label>First Name</Form.Label>
				<Form.Control
					type="firstName"
					placeholder="Enter first name"
					value={firstName}
					onChange={e => setFirstName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="lastName">
				<Form.Label>Last Name</Form.Label>
				<Form.Control
					type="lastName"
					placeholder="Enter last name"
					value={lastName}
					onChange={e => setLastName(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
			</Form.Group>

			<Form.Group controlId="mobileNo">
				<Form.Label>Mobile Number</Form.Label>
				<Form.Control
					type="mobileNo"
					placeholder="Enter Mobile Number"
					value={mobileNo}
					onChange={e => setMobileNo(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password1"
					placeholder="Password"
					value={password1}
					onChange={e => setPassword1(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password2"
					placeholder="Verify Password"
					value={password2}
					onChange={e => setPassword2(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ? 
				<Button variant="primary my-3" type="submit" id="submitBtn">Submit</Button>
				:
				<Button variant="danger my-3" type="submit" id="submitBtn" disabled>Submit</Button>
			}
			
		</Form>
	)
}