//import { Link } from 'react-router-dom';
import Banner from '../components/Banner';

export default function Error(){

	const data = {
		title: "Error 404 - Page Not Found!",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back to Home"
	};

	return (
		<Banner data={data} />
	);
};

/*	return(
		<div>
			<h1>Page Not Found</h1>
			<p>The page you are looking for does not exist.</p>
			<p>
				Go back to the <Link to="/">homepage</Link>.
			</p>
		</div>
	);
};*/