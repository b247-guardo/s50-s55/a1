import { Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';
//import { useState, useEffect } from 'react';

import { Link } from 'react-router-dom';

export default function CourseCard({course}) {

	// Checks to see if the data was successfully passed
	//console.log(props);
	// Every component receives information in a form of an object
	//console.log(typeof props);

	// Destructure the "course" properties into their own variables "course" to make the code even shorter
	const {name, description, price, _id} = course;

	// Use the state hook for this component to be able to store its state
	// States are used to keep track of information related to individual components

	// Syntax
		// const [ getter, setter ] = useState(initialGetterValue);
	//const [count, setCount] = useState(0);
	//const [seats, setSeats] = useState(30);
	//const [isOpen, setIsOpen] = useState(true);

	//console.log(useState(0));

	// Using the state hook returns an array with the first element being a value and the second element as a function that's used to change the value of the first element.
	/*function enroll(){
		if (seats > 0) {
		setCount(count + 1);
		console.log('Enrollees ' + count);
		setSeats(seats - 1);
		console.log('Seats: ' + seats);
		} else {
			alert("No more seats available.");
		}*/
		/*if (count === 30) {
			alert('No more seats.');
			return;
		}*/	
	//};

	/*function enroll(){
		setCount(count + 1);
		console.log('Enrollees ' + count);
		setSeats(seats - 1);
		console.log('Seats: ' + seats);
	}*/

	// useEffect - allows us to instruct the app that the component needs to do something after render.

	/*useEffect(() => {
		if (seats === 0){
			setIsOpen(false);
			alert("No more seats available.");
			document.querySelector(`#btn-enroll-${id}`).setAttribute('disabled', true);
		}*/

		// will run anytime one of the values in the array of dependencies changes.
	//}, [seats]);

		// Three types of dependencies
			// 1. No dependency - effect functions will run every time the component renders.
			// 2. With dependency (empty array) - effect function will only run (one time) when the components mounts and unmounts.
			// 3. With dependency = effect function will run any time one of the values in the array of dependencies changes.

	return (
		<Card className="my-3">
			<Card.Body>
				<Card.Title>{name}</Card.Title>
					<Card.Subtitle>Description:</Card.Subtitle>
					<Card.Text>{description}</Card.Text>
					<Card.Subtitle>Price:</Card.Subtitle>
					<Card.Text>{price}</Card.Text>
					<Button className="bg-primary" as={Link} to={`/courses/${_id}`} > Details </Button>
			</Card.Body>
		</Card>
	);
};

// Check if the CourseCard component is getting the correct prop types
// PropTypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is passed from one component to the next
CourseCard.propTypes = {
		course: PropTypes.shape({
		// Define the properties and their expected types
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}